﻿// TreeViewListView.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TreeViewListView.h"
#include <windowsx.h>
#include <WinUser.h>
#include <wchar.h>

#define MAX_LOADSTRING 100
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
LPCWSTR GetPath(HTREEITEM hItem);
LPCWSTR GetPathList(int iItem);

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TREEVIEWLISTVIEW, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TREEVIEWLISTVIEW));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TREEVIEWLISTVIEW));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TREEVIEWLISTVIEW);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//

HWND listview;
HWND treeview;
HTREEITEM hRoot;
TV_INSERTSTRUCT tvInsert;
#define BUFFER_LEN 105

#define KB 1
#define MB 2
#define GB 3
#define TB 4
#define RADIX 10

LPWSTR Convert(__int64 nSize)
{
	int nType = 0; //Bytes

	while (nSize >= 1048576) //
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		//Lấy một chữ số sau thập phân của nSize chứa trong nRight
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, RADIX);

	if (nRight != 0 && nType > KB)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, RADIX);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0://Bytes
		StrCat(buffer, _T(" bytes"));
		break;
	case KB:
		StrCat(buffer, _T(" KB"));
		break;
	case MB:
		StrCat(buffer, _T(" MB"));
		break;
	case GB:
		StrCat(buffer, _T(" GB"));
		break;
	case TB:
		StrCat(buffer, _T(" TB"));
		break;
	}

	return buffer;
}

LPWSTR GetDateModified(const FILETIME &ftLastWrite)
{

	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d %s"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour>12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour>12) ? (_T("Chiều")) : (_T("Sáng")));

	return buffer;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId = LOWORD(wParam);
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);

	case WM_SETFOCUS:
		SetFocus(listview);
		SetFocus(treeview);
		break;
	case WM_SIZE:
		MoveWindow(listview, 300, 0, LOWORD(lParam), HIWORD(lParam), TRUE);
		MoveWindow(treeview, 0, 0, 300, HIWORD(lParam), TRUE);
		break;

	case WM_NOTIFY:
	{
					  HTREEITEM hSelect;
					  int index;
					  WCHAR buffer[255];
					  switch (wmId)
					  {
					  case IDC_TREEVIEW:
					  {
										   LPNMHDR info = (LPNMHDR)lParam;
										   if (NM_DBLCLK == info->code) {
											   hSelect = TreeView_GetNextItem(treeview, -1, TVGN_CARET);

											   WCHAR* buffer;
											   buffer = new WCHAR[MAX_PATH];
											   LPCWSTR path = GetPath(hSelect);
											   StrCpy(buffer, path);

											   StrCat(buffer, _T("\\*"));

											   TV_INSERTSTRUCT tvInsert;
											   tvInsert.hParent = hSelect;
											   tvInsert.hInsertAfter = TVI_LAST;
											   tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;


											   WIN32_FIND_DATA fd;
											   HANDLE hFile = FindFirstFileW(buffer, &fd);
											   BOOL bFound = 1;

											   if (hFile == INVALID_HANDLE_VALUE)
												   bFound = FALSE;

											   TCHAR * folderPath;
											   while (bFound)
											   {
												   if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
													   && ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
													   && ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
													   && (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
												   {
													   tvInsert.item.pszText = fd.cFileName;
													   folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];

													   StrCpy(folderPath, path);
													   if (wcslen(path) != 3)
														   StrCat(folderPath, _T("\\"));
													   StrCat(folderPath, fd.cFileName);

													   tvInsert.item.lParam = (LPARAM)folderPath;
													   HTREEITEM hItem = TreeView_InsertItem(treeview, &tvInsert);
												   }

												   bFound = FindNextFileW(hFile, &fd);
											   }//while
										   }
										   break;
					  }
					  case IDC_LISTVIEW:
					  {
										   LPNMHDR info1 = (LPNMHDR)lParam;
										   if (NM_DBLCLK == info1->code)
										   {
											   index = ListView_GetNextItem(listview, -1, LVNI_FOCUSED);
											   WCHAR* buffer;
											   buffer = new WCHAR[MAX_PATH];
											   LPCWSTR path = GetPathList(index);
											   StrCpy(buffer, path);

											   StrCat(buffer, _T("\\*"));

											   LVITEM item;
											   item.mask = LVFIF_TEXT | LVIF_PARAM;
											   item.iSubItem = 0;
											   item.cchTextMax = 255;
											   item.iItem = index;

											   WIN32_FIND_DATA fd;
											   HANDLE hFile = FindFirstFileW(buffer, &fd);
											   BOOL bFound = 1;

											   if (hFile == INVALID_HANDLE_VALUE)
												   bFound = FALSE;

											   ListView_DeleteAllItems(listview);
											   TCHAR * folderPath;
											   while (bFound)
											   {

												   if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
													   && ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
													   && ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
													   && (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
												   {
													   item.pszText = fd.cFileName;
													   folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];

													   StrCpy(folderPath, path);
													   if (wcslen(path) != 3)
														   StrCat(folderPath, _T("\\"));
													   StrCat(folderPath, fd.cFileName);

											
													   
													   item.lParam = (LPARAM)folderPath;
													   ListView_InsertItem(listview, &item);
													 
													   ListView_SetItemText(listview, index, 2, _T("Thư mục"));

													   //Cột thứ hai là cột Date modified
													   ListView_SetItemText(listview, index, 1, GetDateModified(fd.ftLastWriteTime));
													   
													  
													   
												   }

												  

												   bFound = FindNextFileW(hFile, &fd);
											   }//while

											   int nItemCount = 0;
											   DWORD folderCount = nItemCount;
											   
											   TCHAR *filePath;
											   DWORD fileSizeCount = 0;
											   DWORD fileCount = 0;

											   hFile = FindFirstFileW(buffer, &fd);
											   bFound = TRUE;

											   if (hFile == INVALID_HANDLE_VALUE)
												   bFound = FALSE;

											   while (bFound)
											   {
												   if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
													   ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
													   ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
												   {
													   filePath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
													   StrCpy(filePath, path);

													   if (wcslen(path) != 3)
														   StrCat(filePath, _T("\\"));

													   StrCat(filePath, fd.cFileName);

													   //Cột thứ nhất là tên hiển thị của tập tin
													   item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
													   item.iItem = nItemCount;
													   item.iSubItem = 0;
													   item.pszText = fd.cFileName;
													   item.lParam = (LPARAM)filePath;
													  
													   ListView_InsertItem(listview, &item);
													   DWORD dwSize = fd.nFileSizeLow;
													   LPWSTR size = Convert(dwSize);

													   //Cột thứ tư là Size
													   ListView_SetItemText(listview, nItemCount, 3, size);
													   fileSizeCount += fd.nFileSizeLow;

												

													   //Cột thứ hai là Date Modified	
													   ListView_SetItemText(listview, nItemCount, 1, GetDateModified(fd.ftLastWriteTime));

													   ++nItemCount;
													   ++fileCount;
												   }//if

												   bFound = FindNextFileW(hFile, &fd);
											   }//while
										   }
										   break;
					  }
					  }
					  break;
	}
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	INITCOMMONCONTROLSEX icex;

	// Ensure that the common control DLL is loaded. 
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(20, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	listview = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
		WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_AUTOHSCROLL | ES_AUTOVSCROLL | LVS_REPORT | WS_BORDER,
		0, 0, 0, 0, hWnd, (HMENU)IDC_LISTVIEW, hInst, NULL);

	// Tạo ra 3 cột
	LVCOLUMN lvCol1;
	lvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol1.fmt = LVCFMT_LEFT;
	lvCol1.pszText = L"Name";
	lvCol1.cx = 200;
	ListView_InsertColumn(listview, 0, &lvCol1);

	LVCOLUMN lvCol2;
	lvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol2.fmt = LVCFMT_LEFT;
	lvCol2.pszText = L"Date Modified";
	lvCol2.cx = 200;
	ListView_InsertColumn(listview, 1, &lvCol2);

	LVCOLUMN lvCol3;
	lvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol3.fmt = LVCFMT_LEFT;
	lvCol3.pszText = L"Type";
	lvCol3.cx = 200;
	ListView_InsertColumn(listview, 2, &lvCol3);

	LVCOLUMN lvCol4;
	lvCol4.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol4.fmt = LVCFMT_LEFT;
	lvCol4.pszText = L"Size";
	lvCol4.cx = 200;
	ListView_InsertColumn(listview, 3, &lvCol4);

	treeview = CreateWindowEx(NULL, WC_TREEVIEWW, L"",
		WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_AUTOHSCROLL | ES_AUTOVSCROLL | WS_BORDER|TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS,
		0, 0, 0, 0, hWnd, (HMENU)IDC_TREEVIEW, hInst, NULL);

	//Node gốc
	
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.mask = TVIF_TEXT | TVIF_CHILDREN;
	tvInsert.item.pszText = L"This PC";
	tvInsert.item.cChildren = TRUE;
	hRoot = TreeView_InsertItem(treeview, &tvInsert);

	TCHAR buffer[BUFFER_LEN];
	int i;
	int m_nCount = 0;
	GetLogicalDriveStrings(BUFFER_LEN, buffer);

	//Đếm số lượng ổ đĩa 
	for (i = 0; !((buffer[i] == 0) && (buffer[i + 1] == 0)); ++i)
	{
		if (buffer[i] == 0)
			++m_nCount;
	}
	++m_nCount;

	int j, k;
	WCHAR *m_pszDrive;

	i = 0;
	//Lấy từng tên ổ đĩa
	for (j = 0; j< m_nCount; ++j)
	{
		m_pszDrive = new WCHAR[6];
		k = 0;
		while (buffer[i] != 0)
		{
			m_pszDrive[k++] = buffer[i++];
		}
		m_pszDrive[k-1] = 0; //Kết thúc chuỗi
		++i;

		tvInsert.hParent = hRoot;
		tvInsert.hInsertAfter = TVI_LAST;
		tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_CHILDREN;
		tvInsert.item.pszText = m_pszDrive;
		tvInsert.item.cChildren = TRUE;
		tvInsert.item.lParam = (LPARAM)m_pszDrive;
		TreeView_InsertItem(treeview, &tvInsert);
		TreeView_Expand(treeview, hRoot, TVE_EXPAND);

		LV_ITEM lv;
		lv.mask = LVIF_TEXT | LVIF_PARAM;
		lv.iItem = j;
		lv.iSubItem = 0;
		lv.pszText = m_pszDrive;
		lv.lParam = (LPARAM)m_pszDrive;
		ListView_InsertItem(listview, &lv);
	}



	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	// Parse the menu selections:
	switch (id)
	{

	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

LPCWSTR GetPath(HTREEITEM hItem)
{
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(treeview, &tv);
	return (LPCWSTR)tv.lParam;
}

LPCWSTR GetPathList(int iItem)
{
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(listview, &lv);
	return (LPCWSTR)lv.lParam;
}