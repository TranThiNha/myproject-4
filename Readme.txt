Họ tên: Trần Thị Nhã
MSSV: 1512364
Email: trannha132@gmail.com
Các chức năng đã làm được:
- Tạo ra TreeView bên trái, ListView bên phải. 
Xét TreeView:
- Tạo node root là This PC.
- Lấy danh sách các ổ đĩa trong máy, thêm các ổ đĩa vào node root. 
- Duyệt nội dung thư mục bằng FindFirstFile & FindNextFile, chỉ lấy các thư mục để thêm vào làm node con.
Xét ListView:
- Hiển thị toàn bộ thư mục và tập tin tương ứng với một đường dẫn
- Bấm đôi vào một thư mục sẽ thấy toàn bộ thư mục con và tập tin.
- Tạo ra ListView có 4 cột: Tên, Loại, Thời gian chỉnh sửa, Dung lượng. 
- Với thư mục đã set text cho 2 cột Tên và Loại. 
- Với tập tin hiển thị 3 cột: Tên, Thời gian chỉnh sửa, Kích thước.
Các luồng sự kiện chính:
Xét TreeView:
- Chạy chương trình lên, hiển thị node This PC trên TreeView bên trái ở trạng thái thu gọn. Bấm vào sẽ xổ xuống các node con là danh sách ổ đĩa.
- Bấm đúp vào ổ đĩa C đang ở trạng thái thu gọn trong TreeView bên trái sẽ xổ xuống danh sách các thư mục con.
- Bấm đúp vào thư mục trong ổ đĩa bất kì, trong TreeView bên trái sẽ xổ xuống các thư mục nằm trong thư mục đó (nếu có).
Xét ListView:
- Chạy chương trình lên, hiển thị 3 ổ đĩa trong cột Name.
- Bấm đúp vào một ổ đĩa trong ListView bên phải sẽ hiện ra các thư mục và tập tin với các thông tin Tên, Kích thước, Thời gian chỉnh sửa, Loại.
- Bấm đúp vào một thư mục trong ListView, trong ListView bên phải sẽ hiện ra các thư mục và tập tin bên trong nó với các thông tin Tên, Kích thước, Thời gian chỉnh sửa, Loại (nếu có).
Link Bitbucket: https://TranThiNha@bitbucket.org/TranThiNha/myproject-4.git
Link youtube: https://youtu.be/NuPXU5HT00I